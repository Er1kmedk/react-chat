const express = require('express');
const app = express();
const { PORT = 4000 } = process.env;
const cors = require('cors')
const jwt = require('jsonwebtoken')

// const http = require('http').createServer(app);
// const io = require('socket.io')(http);

// The chat message bounce
// io.on('connection', socket => {
//     socket.on('message', ({ name, message }) => {
//         io.emit('message', { name, message })
//     })
// })

// http.listen(PORT, function() {
//     console.log(`Listening to port ${ PORT }`);
// })


app.use(express.json())
app.use(cors())


let accessTokens = []
console.log(accessTokens.length);


// The endpoint used for refreshing the user accessability after the initally
// generated token has expired.
// app.post('/token', (req, res) => {
//     const refreshToken = req.body.token

//     if (refreshToken == null) return res.sendStatus(401)
//     if (!accessTokens.includes(refreshToken)) return res.sendStatus(403)
    
//     jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, (err, user) => {
//         if (err) return res.sendStatus(403)
//         const accessToken = generateAccessToken({ name: user.name })
//         res.json({ accessToken: accessToken })
//     })
// })

// The endpoint to deffinetly close user access. This is done by deleting the 
// refresh token used to maintain access. 
app.delete('/logout', (req, res) => {
    accessTokens = accessTokens.filter(token => token !== req.body.token)
    console.log(accessTokens.length);
    res.sendStatus(202)
})

// The endpoint for initial login
app.post('/login', (req, res) => {
    const username = req.body.username
    const user = { name: username }    

    const accessToken = generateAccessToken(user)
    accessTokens.push(accessToken);
    console.log(accessTokens.length);
    // accessTokens.forEach(element => console.log(element));

    // const refreshToken = jwt.sign(user, process.env.REFRESH_TOKEN_SECRET)
    // refreshTokens.push(refreshToken)
    // console.log(refreshTokens.length);
    // refreshTokens.forEach(element => console.log(element));

    // Return a primary accesstokena as well as a refresh token that later
    // can be used to maintain user accessability
    // res.json({ accessToken: accessToken, refreshToken: refreshToken})
    res.json({ accessToken: accessToken })
})

function generateAccessToken(user) {
    // return jwt.sign(user, "bfa93fa1bff12cb5c31186a7264efb77ec8bb24c8d336eaf2939c1f2b695aee4ebb1e1b77a9fb82", {expiresIn: '30s'})
    return jwt.sign(user, "bfa93fa1bff12cb5c31186a7264efb77ec8bb24c8d336eaf2939c1f2b695aee4ebb1e1b77a9fb82")
}

app.get('/ee', (req, res) => {
    console.log("hej")
    res.status(200).send('Elroj') 
})


app.listen(PORT, () => console.log(`Server started on port ${PORT}`))