import React from 'react';
import './App.css';
import Registation from './Registration';
import Chat from './Chat';
// import { Router, Route } from 'react-router-dom';
import {
  BrowserRouter as Router,
  Route
} from "react-router-dom";


function App() {
  return (
    <div className="App">
      
    <Router>
      <Route exact path= '/' component={ Registation }></Route>
      <Route path={'/chat'} component={ Chat }></Route>
    </Router>
     
    </div>
  );
}

export default App;
