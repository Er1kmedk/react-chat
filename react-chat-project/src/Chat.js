import React, { useState, useEffect } from 'react';
import io from 'socket.io-client';
import TextField from '@material-ui/core/TextField';

// OBS Localhost 4k represents chat server backend
// which has to be started separatly
// Denna connect får bara göras EFTER authentication!

const socket = null
function Chat() {
    const [state, setState] = useState({message: '', name: ''})
    const [chat, setChat] = useState([])
    console.log("this.props.location.state.accessToken");
  
    useEffect(() => {
        socket = io.connect('http://localhost:4000', {
    transportOptions: {
      polling: {
        extraHeaders: {
          'custom': 'hello'
        }
      }
    }
  })

      socket.on('message', ({ name, message }) => {
        setChat([ ...chat, { name, message }])
      })
    })

    const onTextChange = e => {
        setState({ ...state, [e.target.name]: e.target.value })
    }

    const onMessageSubmit = e => {
        e.preventDefault()
        const { name, message } = state
        socket.emit('message', { name, message })
        setState({ message: '', name })
    }

    // The renderChat() function is being called at the bottom.
    const renderChat = () => {
        return chat.map(({ name, message }, index) => (
            <div key={index}>
                <h3>
                    {name}: <span>{message}</span>
                </h3>
            </div>
        ))
    }

    return (
        <div className="Chat">
            <form onSubmit={onMessageSubmit}>
                <h1>Messanger</h1>
                <div>
                    <TextField
                        name="name"
                        onChange={e => onTextChange(e)}
                        value={state.name}
                        label="Name"
                    ></TextField>
                </div>

                <div>
                    <TextField
                        name="message"
                        onChange={e => onTextChange(e)}
                        value={state.message}
                        label="Message"
                    ></TextField>
                </div>

                <button>Send Message</button>
            </form>

            <div>
                <h1>Chat Log</h1>
                {renderChat()}
            </div>
        </div>
    );
}

export default Chat;