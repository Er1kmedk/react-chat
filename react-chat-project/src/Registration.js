import React, { Component } from 'react';


export default class Registration extends Component {

    

    constructor(props) {
        super(props);

        this.state = {
            username: "",
            password: "",
        }

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    async handleSubmit(event) {
        const tokenFromAPI = await fetch('http://localhost:4000/login', {
            method: 'POST',
            headers: {
                username: this.state.username
              },
        })
        .then((resp) => resp.json())
        .then((resp) => {
            const accessToken = resp.accessToken;
            // console.log(accessToken);
            // Redirect to /Chat route
            return accessToken
        })
        .catch(err => console.log(err))
        
        console.log(tokenFromAPI);

        let data = {
            username: this.state.username,
            password: this.state.password,
            accessToken: tokenFromAPI
        }

        this.props.history.push('/chat', data)
    }

    render() {
        return (
            <div>
                <input 
                    type="text" 
                    name="username" 
                    placeholder="Username" 
                    value={this.state.username} 
                    onChange={this.handleChange} 
                    required>
                </input>

                <input 
                    type="password" 
                    name="password" 
                    placeholder="Password" 
                    value={this.state.password} 
                    onChange={this.handleChange} 
                    required>
                </input>

                <button onClick={(e) => this.handleSubmit(e)}>Register</button>
            </div>
        )
    }
}